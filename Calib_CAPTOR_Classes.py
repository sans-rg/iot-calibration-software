
"""
Created on Wed Nov  8 12:13:28 2017

    Captor Library

@author: pauTE
"""

import numpy as np
import matplotlib.pyplot as plt
import csv
import copy
from datetime import datetime
from numpy.linalg import inv
from sklearn import linear_model
from sklearn.neighbors import KNeighborsRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn import svm
from sklearn.model_selection import GridSearchCV


class DataSet:

    def __init__(self, TRAIN_PERCENTAGE = 0.65):
        self.TRAIN_PERCENTAGE = TRAIN_PERCENTAGE

    def READ_DATA(self,datafile):
        '''
        This function takes a datafile with samples and creates a list:
        each item has the columns of the CSV file (being the first one
        the date and the rest sensor's readings, e.g. O3, T, HR).
        '''
        with open(datafile) as f:
            ncols = len(f.readline().split(';'))
        print(ncols)
        Data = []
        with open(datafile) as f:
            next(f)
            reader=csv.reader(f,delimiter=';')
            for row in reader:
                row_p = []
                row_p.append(row[0])
                lista = [float(s) for s in row[1:]]
                row_p.extend(lista)
                Data.append(row_p)
        return Data



    def NORMALIZE(self,t_Data,t_Mean,t_Sd):
        '''
        This function normalizes the data with respect a mean and std.
        '''
        t_Norm=np.zeros(t_Data.shape)
        for i in range(0,t_Data.shape[1]):
            if t_Sd[i]!=0.:
                t_Norm[:,i]=(t_Data[:,i]-t_Mean[i])/t_Sd[i]
            else:
                t_Norm[:,i]=t_Data[:,i]
        return t_Norm





    def DATA_DICT(self,datafile,ER,sensors_cal,Temp,HR):
        '''
        This function creates a dictionary with the datafile.
        '''
        tr_dict=dict()
        tr_dict["O_PR"]= datafile[:,ER]  # vector with O3_PR
        tr_dict["HR_Captor"]=datafile[:,HR]  # vector with HR captor node
        tr_dict["Temp_Captor"]=datafile[:,Temp]  # vector with Temp captor node
        for i in sensors_cal:
            captor="O_Captor_"+str(i)
            tr_dict[captor]=datafile[:,i]  # adding O3 of captor sensors
        return tr_dict

    def errors(self,x_PD,y_Data,tMean,p_s):
        '''
        This function obtains the RMSE and R2 QoI metrics.
        '''
        error_vector_PD=(x_PD-y_Data)**2
        errorMSE_PD = np.sum(error_vector_PD)
        RMSE = np.sqrt(errorMSE_PD/(len(x_PD)-p_s+1))

        avg_vector_PD=(tMean-y_Data)**2
        avg = np.sum(avg_vector_PD)
        R2 = 1.0-errorMSE_PD/avg
        return RMSE,R2

    def errors_cut(self,x_PD,y_Data,tMean,p_s,LOW_LIMIT):
        '''
        This function obtains the RMSE and R2 QoI metrics.
        '''
        error_vector = []
        for i in range(len(x_PD)):
            if x_PD[i] >= LOW_LIMIT:
                error_vector.append(x_PD[i]-y_Data[i])
#        print 'ORG = ',len(x_PD), 'CUT = ', len(error_vector)
        error_vector = np.array(error_vector)
        error_vector_PD=(error_vector)**2
        errorMSE_PD = np.sum(error_vector_PD)
        RMSE = np.sqrt(errorMSE_PD/(len(error_vector)-p_s+1))
#==============================================================================
#
#         avg_vector_PD=(tMean-y_Data)**2
#         avg = np.sum(avg_vector_PD)
#         R2 = 1.0-errorMSE_PD/avg
#==============================================================================
        return RMSE

    def Data_Model(self,xvector,yvector):
        '''
        This function obtains a model.
        xvector = list with X variables
        yvector = the Reference station data
        '''
        X= np.matrix(xvector)
        X=np.transpose(X)
        Y=np.matrix(yvector)
        Y=np.transpose(Y)
        return X,Y



    def train_tune_KNN(self, X_des, Y_des):

        """ This function performs parameter tunning for KNN
          and trains the model with the best set of parameters """
        kfold = 10
        # 10-fold cross-validation strategy is used
        neighs = np.arange(start = 1, stop = 50, step = 2)
        ps = np.arange(start = 1, stop = 9, step = 1)
        param_grid = { 'n_neighbors' : neighs, 'p': ps}
        knn_estimator = KNeighborsRegressor()
        search = GridSearchCV(knn_estimator, param_grid,scoring = 'neg_mean_squared_error', cv = kfold, refit = False )
        search.fit(X_des, Y_des)
        cv_error = search.best_score_
        best_k = search.best_params_['n_neighbors']
        best_p = search.best_params_['p']
        knn_estimator = KNeighborsRegressor(n_neighbors = best_k, p = best_p )
        knn_estimator.fit(X_des, np.ravel(Y_des))

        return knn_estimator, [best_k, best_p, cv_error]

    def train_tune_RF(self, X_des, Y_des):

        """ This function performs parameter tunning for RF
          and trains the model with the best set of parameters """
        kfold = 10
        # 10-fold cross-validation strategy is used
        n_estimators = [16 , 32, 50, 100, 250, 500, 660, 1000]
        max_features = [1,2,3]
        max_depth = [3,5,7,10]
        param_grid = { 'n_estimators' : n_estimators, 'max_features': max_features,
                      'max_depth': max_depth}
        rf_estimator = RandomForestRegressor()
        search = GridSearchCV(rf_estimator, scoring = 'neg_mean_squared_error', param_grid = param_grid, cv = kfold, refit = False )
        search.fit(X_des, np.ravel(Y_des))
        cv_error = search.best_score_
        best_n = search.best_params_['n_estimators']
        best_features = search.best_params_['max_features']
        best_depth = search.best_params_['max_depth']
        rf_estimator = RandomForestRegressor(n_estimators = best_n,
                                             max_features = best_features,
                                             max_depth = best_depth)
        rf_estimator.fit(X_des, np.ravel(Y_des))
        return rf_estimator, [best_n, best_features, best_depth, cv_error]

    def train_tune_SVR(self, X_des, Y_des):

        """ This function performs parameter tunning for SVR
          and trains the model with the best set of parameters """
        kfold = 10
        # 10-fold cross-validation strategy is used
        cs = [1.0, 10.0, 1000.0]
        gs = np.arange(start=0.1, stop = 2.0, step=0.2)
        epsilons = np.arange( start = 0.05, stop = 0.25, step = 0.05)
        param_grid = { 'C' : cs, 'gamma': gs, 'epsilon': epsilons }
        #The impact of the gamma parameter is much higer than the cost parameter
        # so we check more values of gamma
        # We must also tune the epsilon from epsilon-insensitive loss function
        svr_estimator = svm.SVR()
        search = GridSearchCV(svr_estimator, scoring = 'neg_mean_squared_error',param_grid = param_grid, cv = kfold, refit = True )
        search.fit(X_des, Y_des)
        cv_error = search.best_score_
        best_c = search.best_params_['C']
        best_gamma = search.best_params_['gamma']
        best_eps = search.best_params_['epsilon']
        svr_estimator = svm.SVR(C = best_c, gamma = best_gamma,
                                epsilon = best_eps)
        svr_estimator.fit(X_des, np.ravel(Y_des))

        return search, [best_c, best_gamma, best_eps, cv_error]



#%%
class Utils_Cal:



    def shift(self,a,step):
        '''
        This function shifts the data if this is not UTC with respect ER data.
        '''
        ee=[0.0]*len(a)
        for t in range(len(a)):
            ee[t]=a[t]
        if step>0:
            for t in range(step,len(a)):
                ee[t]=a[t-step]
        if step<0:
            for t in range(len(a)+step):
                ee[t]=a[t-step]
        return ee


#%%
class Plots_Cal:

    def __init__(self,date_in,date_end,node_id,place):
        self.color = ['black','red','blue','magenta','green','orange']
        self.date_in = str(date_in)
        self.date_end = str(date_end)
        self.node_id = node_id     ## CAPTOR number
        self.place = place
        self.sensors = ['ER','s1','s2','s3','s4','s5']

    def PLOT_NORM_DATA_SENSORS(self,NormDataSet,file_wr,sens,saved,ylabel):
        '''
        This figure plots the normalized data for the n sensors
        sens = [0,1,2,3,4,5] or [0,3] indicates the columns to be plotted
        saved = TRUE or FALSE if wanted to be saved in a file
        '''
        plt.ion()
        plt.figure(1)
        ll=0
        ul=len(NormDataSet)
        x = np.arange(ll, ul, 1)
        for i in sens:
            plt.plot(x,NormDataSet[:ul,i],color=self.color[i],linewidth=2.0, linestyle="-",label=self.sensors[i])
        plt.ylabel('Normalized' + ylabel + ' values',fontsize=13)
        text = self.node_id + ': Number of samples (1 sample each hour)'
        plt.xlabel(text,fontsize=13)
        plt.legend(loc='upper left')
        plt.title(self.place+" - Dates, from:"+self.date_in+" to "+self.date_end)
        if saved:
            plt.savefig(file_wr)
        plt.show()
        plt.close(1)

    def PLOT_DATA_SENSORS(self,DataSet,file_wr,sens,saved,ylabel):
        '''
        This figure plots the data for the n sensors
        sens = [0,1,2,3,4,5] or [0,3] indicates the columns to be plotted
        saved = TRUE or FALSE if wanted to be saved in a file
        '''
        plt.ion()
        plt.figure(2)
        ll=0
        ul=len(DataSet)
        x = np.arange(ll, ul, 1)
        for i in sens:
            plt.plot(x,DataSet[:ul,i],color=self.color[i],linewidth=2.0, linestyle="-",label=self.sensors[i])
        plt.ylabel('Resistor measure of an' + ylabel + ' sensor (KOhm)',fontsize=13)
        text = self.node_id + ': Number of samples (1 sample each hour)'
        plt.legend(loc='upper left')
        plt.xlabel(text,fontsize=13)
        plt.title(self.place+" - Dates, from:"+self.date_in+" to "+self.date_end)
        if saved:
            plt.savefig(file_wr)
        plt.show()
        plt.close(2)

    def SCATTERPLOT_NORM_DATA_SENSORS(self,DataS_RS,DataS_Sn,file_wr,sensor,saved,ylabel):
        '''
        This figure plots the data for the n sensors
        DataS_RS is the Ref Station data
        DataS_Sn is the sensor data
        sensor =  indicates the sensor to be scatter-plotted
        saved = TRUE or FALSE if wanted to be saved in a file
        '''
        plt.ion()
        plt.figure(3)
        plt.scatter(DataS_Sn,DataS_RS,color=self.color[sensor],label='s'+str(sensor))
        plt.xlabel(self.node_id + ': Sensor' + ylabel + ' (normalized data)',fontsize=13)
        plt.ylabel('Reference node' + ylabel + ' (normalized data)',fontsize=13)
        plt.legend(loc='upper left')
        if saved:
            plt.savefig(file_wr)
        plt.show()
        plt.close(3)




    def PLOT_CALIB_DATA_SENSORS(self,x,ER_data,S_data,file_wr,sensor,labels,saved,max_y):
        '''
        This figure plots the calibrated (training, test or All-data) data for the n sensors
        ER_data is the Ref Station data
        S_data is the sensor data
        labels = labels to be printed [xlabel,title]
        saved = TRUE or FALSE if wanted to be saved in a file
        '''
        plt.figure(4)
        plt.ion()
        plt.plot(x,ER_data,color=self.color[0],linewidth=1.75, linestyle="-",label=self.place)
        plt.plot(x,S_data,color=self.color[labels[3]], linewidth=1.75, linestyle="-",label=self.node_id+self.sensors[sensor])
        plt.ylabel(labels[2] +' $\mu$gr/m$^3$')
        plt.ylim(0,max_y+10)
        plt.xlabel(labels[0],fontsize=13)
        plt.legend(loc='lower right')
        plt.title(labels[1],fontsize=13)
        if saved:
            plt.savefig(file_wr,bbox_inches='tight')
        plt.show()
        plt.close(4)

    
