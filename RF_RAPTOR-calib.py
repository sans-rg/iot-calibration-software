# -*- coding: utf-8 -*-
"""
Created on Tue Sep 18 20:44:17 2018

@author: pauTE
"""

#%%
import numpy as np
import matplotlib as mpl
from sklearn.utils import shuffle
import csv
from Calib_CAPTOR_Classes import Plots_Cal,Utils_Cal,DataSet
import os
import argparse
from argparse import RawTextHelpFormatter
import sys
from time import time
import pickle
# Export as dot file
parser = argparse.ArgumentParser(description='Calibrate a Raptor Node with 1 Ozone Sensor and 1 No2',formatter_class=RawTextHelpFormatter)
parser.add_argument("-f", "--raptor_file", default=None, help = 'file with the CSV data')
parser.add_argument("-n", "--raptor_name", default=None, help = 'Name of the Raptor')
parser.add_argument("-p", "--Place_ER", default=None, help = 'Name of the Reference Station')
parser.add_argument("-s", "--sensors_c", default=2, help = 'Sensors to calibrate, e.g. 134, 1=s1, 3=s3, and 4=s4')
parser.add_argument("-t", "--TOTAL_SEN", default=5, help = 'Total number of sensrs between the Ref Station and the Temp/HR sensor')
parser.add_argument("-w", "--TRAIN_PERCENTAGE", default=75, help = 'Pertentage of the Training set')
parser.add_argument("-y", "--YEAR", default=2017, help = 'Campaing year')


# Argument parsing
args = parser.parse_args()
raptor_file = args.raptor_file
raptor_name = args.raptor_name
Place_ER = args.Place_ER
sensors_c = args.sensors_c
TOTAL_SEN = 2 # In a Raptor file there are only two pollution sensors, O3 and NO2
TRAIN_PERCENTAGE = float(args.TRAIN_PERCENTAGE)/100.0
YEAR = str(args.YEAR)

# Check Errors
def error():
    error1 = "Please, specify the name file with the CSV data"
    error2 = "Please, specify the Name of the Raptor"
    error3 = "Please, specify the Name of the Reference Station"
    error4 = "Please, specify the sensors that you want to calibrate"
    error5 = "Please, specify the TOTAL # of sensors between the Ref Station and the Temp/HR sensor, i.e. number of Ozone/NO2 sensors in the array"
    error6 = "Please, specify the percentage of the training set, e.g. 65 (in %)"
    error7 = "Please, specify Raptor campaign year, e.g. 2017"
    noerror = '1'
    if raptor_file is None:
        return error1
    elif raptor_name is None:
        return error2
    elif Place_ER is None:
        return error3
    elif sensors_c is None:
        return error4
    elif TOTAL_SEN is None:
        return error5
    elif TRAIN_PERCENTAGE is None:
        return error6
    elif YEAR is None:
        return error7
    else:
        return noerror

chk_err = error()
if chk_err is not '1':
    print(chk_err)
    sys.exit()

#%%
############# LOAD THE DATA FROM THE .txt INTO THE DATASET list
# Data is in a folder ./Raptors/YEAR/labeled_data
# Results are written in folder ./Raptors/Results_RF/YEAR
dir_path = os.getcwd()
DATA_path = 'Raptors/'+ YEAR + '/labeled_data'
dir_rd = os.path.join(os.path.dirname(__file__), '.', DATA_path)
dir_wr = os.path.join(os.path.dirname(__file__), '.', 'Raptors/Results_RF/' + YEAR)

data_file = dir_rd + "/" +  raptor_file
dir_path = dir_wr
#%%
############################################################################
# SOME CONSTANTS FOR THE WHOLE PROCESS
sensors_cal=[]
for j in xrange(len(sensors_c)):
    sensors_cal.append(int(sensors_c[j]))

nsen=len(sensors_cal)   #  Number of sensor devices
p_s=3     # number of features (O3, HR, Temp)
# Graphical parameters setting
mpl.rcParams['figure.figsize'] = 7, 7

#initializate the class DataSet and obtain the Data from the CSV file
DataS = DataSet(TRAIN_PERCENTAGE)
AllDataCSV = DataS.READ_DATA(data_file)
ini_date = AllDataCSV[0][0]
end_date = AllDataCSV[len(AllDataCSV)-1][0]
#initializate the class Plots_Cal and Utils_Cal
Plots = Plots_Cal(ini_date,end_date,'Captor-',Place_ER)
Utils = Utils_Cal()

AllData = np.genfromtxt(data_file, delimiter=";",skip_header=1,usecols=range(1,7))

############################################################################
# Shift the data if necessary
#AllData[:,0]=Utils.shift(AllData[:,0],0)
############################################################################

# Normalize the data with respect its mean and std
AllDataMean=np.mean(AllData,axis=0)
AllDataSd=np.std(AllData,axis=0)
NormAllData=np.zeros(AllData.shape)
for i in range(0,AllData.shape[1]):
    NormAllData[:,i]=(AllData[:,i]-AllDataMean[i])/AllDataSd[i]

# Shuffle the data and normalize the shuffled data
ShuffleAllData = shuffle(AllData, random_state=0)
NormShuffleAllData=np.zeros(ShuffleAllData.shape)
for i in range(0,ShuffleAllData.shape[1]):
    NormShuffleAllData[:,i]=(ShuffleAllData[:,i]-AllDataMean[i])/AllDataSd[i]

# Obtain the Training (Normalized)
TRAIN = int(len(ShuffleAllData)*TRAIN_PERCENTAGE)
trainData=np.copy(ShuffleAllData[0:TRAIN,])
trainMean=np.mean(trainData,axis=0)
trainSd=np.std(trainData,axis=0)
trainNorm = DataS.NORMALIZE(trainData,trainMean,trainSd)
# Obtain the Test (Normalized)
TEST= len(ShuffleAllData)
testData=np.copy(ShuffleAllData[TRAIN+1:TEST,])
testNorm = DataS.NORMALIZE(testData,trainMean,trainSd)

# All data normalized with respect training meand and training std
Alldata_t_Norm=np.zeros(AllData.shape)
for i in range(0,AllData.shape[1]):
    if trainSd[i]!=0.:
        Alldata_t_Norm[:,i]=(AllData[:,i]-trainMean[i])/trainSd[i]
    else:
        Alldata_t_Norm[:,i]=AllData[:,i]

############################################################################

############  Plot the Sensor's Data
ylabel = 'Ozone'
file_wr = dir_path + "/Data-plot-" + raptor_name + "-all.png"
Plots.PLOT_DATA_SENSORS(AllData,file_wr,[1,2,3,4],'TRUE',ylabel)

############  Plot the Normalized Sensor's Data
file_wr = dir_path + "/DataNorm-plot-" + raptor_name + "-all.png"
Plots.PLOT_NORM_DATA_SENSORS(NormAllData,file_wr,sensors_cal,'TRUE',ylabel)

############################################################################
# Column index in the data matrix for the Reference Station (ER), Temperature
# sensor (Temp) and Relative Humidity sensor (HR)
ER = 0
Temp= 4
HR= 5
# Create a dictionary with the training, test, All and All_Norm data
train_Norm = DataS.DATA_DICT(trainNorm,ER,sensors_cal,Temp,HR)
test_Norm = DataS.DATA_DICT(testNorm,ER,sensors_cal,Temp,HR)
All_data = DataS.DATA_DICT(AllData,ER,sensors_cal,Temp,HR)
All_data_t_Norm = DataS.DATA_DICT(Alldata_t_Norm,ER,sensors_cal,Temp,HR)
y_train_Data_O3=trainData[:,ER]
y_test_Data_O3=testData[:,ER]
meanTest_ER=np.mean(y_test_Data_O3)

############################################################################
# define error variables
train_errorMSE_PD_s=np.zeros(nsen)
train_RMSE_s=np.zeros(nsen)
train_R2_s=np.zeros(nsen)
train_time_s = np.zeros(nsen)
model_ntree_s = np.zeros(nsen)
model_nvar_s = np.zeros(nsen)
model_depth_s = np.zeros(nsen)
cv_s = np.zeros(nsen)
test_errorMSE_PD_s=np.zeros(nsen)
test_RMSE_s=np.zeros(nsen)
test_R2_s=np.zeros(nsen)
y_train_s= np.zeros((nsen, len(y_train_Data_O3)))
y_test_s = np.zeros((nsen, len(y_test_Data_O3)))
y_hat_train_s= np.zeros((nsen, len(y_train_Data_O3)))
y_hat_test_s = np.zeros((nsen, len(y_test_Data_O3)))
tot_errorMSE_PD_s=np.zeros(nsen)
tot_RMSE_s=np.zeros(nsen)
tot_R2_s=np.zeros(nsen)

x_train_PD=np.zeros(len(trainNorm))

#%%
###########################################################################
####                        RF for regression
############################################################################
params_sensors = []
params_sensors.append(raptor_name)
s = -1
i = 2
print "CAPTOR: ", raptor_name , "Calibrating sensor number s= ",i
Ofit="O_Captor_"+str(i)
s=s+1
#  SCATTER-PLOTs of the Normalized Data Sensors
file_wr = dir_path + "/Scatter-plot-" + raptor_name + "-s" + str(i) + ".png"
Plots.SCATTERPLOT_NORM_DATA_SENSORS(NormAllData[:,ER],NormAllData[:,i],file_wr,i,'TRUE',ylabel)

###  TRAINING (RF)
X_des,Y_des = DataS.Data_Model([train_Norm[Ofit],train_Norm["Temp_Captor"],train_Norm["HR_Captor"]],train_Norm["O_PR"])
tiempo_inicial = time()
model, best_params = DataS.train_tune_RF(X_des, Y_des)
tiempo_final = time()
print 'Final TRAINING MODEL'
print(model.get_params())
model_ntree_s[s] = best_params[0]
model_nvar_s[s] = best_params[1]
model_depth_s[s] = best_params[2]
cv_s[s] = best_params[3]
x_train_PD=model.predict(X_des)
x_train_PD=x_train_PD*trainSd[ER]+trainMean[ER]
train_RMSE_s[s],train_R2_s[s] = DataS.errors(x_train_PD,y_train_Data_O3,trainMean[ER],p_s)
y_hat_train_s[s,:] = x_train_PD
y_train_s[s,:] = y_train_Data_O3
print 'TIEMPO DE EJECUCION DEL TRAINING MODEL = ', tiempo_final-tiempo_inicial, ' sec'
###  TEST (RF)
train_time_s[s] = (tiempo_final - tiempo_inicial)
print 'TEST'
X_des,Y_des = DataS.Data_Model([test_Norm[Ofit],test_Norm["Temp_Captor"],test_Norm["HR_Captor"]],test_Norm["O_PR"])
x_test_PD = model.predict(X_des)
x_test_PD = x_test_PD*trainSd[ER]+trainMean[ER]
y_hat_test_s[s,:] = x_test_PD
y_test_s[s,:] = y_test_Data_O3
test_RMSE_s[s],test_R2_s[s] = DataS.errors(x_test_PD,y_test_Data_O3,meanTest_ER,p_s)

###  ALL Data (RF)
X_des,Y_des = DataS.Data_Model([All_data_t_Norm[Ofit],All_data_t_Norm["Temp_Captor"],
                                All_data_t_Norm["HR_Captor"]],All_data_t_Norm["O_PR"])
x_All_PD = model.predict(X_des)
x_All_PD = x_All_PD*trainSd[ER]+trainMean[ER]
tot_RMSE_s[s],tot_R2_s[s] = DataS.errors(x_All_PD,AllData[:,ER],AllDataMean[ER],p_s)

#==============================================================================
# Plot the Training
file_wr = dir_path + "/RF-Training-plot-" + raptor_name + "-s" + str(i) + ".png"
x = np.arange(0, TRAIN, 1)
xlabel =  'Train Set: RMSE = %f ($\mu$gr/m$^3$), R$^2$=%f \n\n' % (train_RMSE_s[s],train_R2_s[s])
title = Place_ER + " - Dates, from:" + ini_date + " to " + end_date
labels = [xlabel,title,'Ozone',1]
max_y = np.max((np.max(x_train_PD),np.max(y_train_Data_O3)))
Plots.PLOT_CALIB_DATA_SENSORS(x,y_train_Data_O3,x_train_PD,file_wr,i,labels,'TRUE',max_y)
#==============================================================================
# Plot the Test
file_wr = dir_path + "/RF-Test-plot-" + raptor_name + "-s" + str(i) + ".png"
x = np.arange(TRAIN+1, TEST, 1)
xlabel =  'Test Set: RMSE = %f ($\mu$gr/m$^3$), R$^2$=%f \n ' % (test_RMSE_s[s],test_R2_s[s])
title = Place_ER + " - Dates, from:" + ini_date + " to " + end_date
labels = [xlabel,title,'Ozone',2]
max_y = np.max((np.max(x_test_PD),np.max(y_test_Data_O3)))
Plots.PLOT_CALIB_DATA_SENSORS(x,y_test_Data_O3,x_test_PD,file_wr,i,labels,'TRUE',max_y)
#==============================================================================
# Plot training and testing together
file_wr = dir_path + "/RF-All-plot-" + raptor_name + "-s" + str(i) + ".png"
x = np.arange(0, TEST, 1)
xlabel =  'All Set: RMSE = %f ($\mu$gr/m$^3$), R$^2$=%f' % (tot_RMSE_s[s],tot_R2_s[s])
title = Place_ER + " - Dates, from:" + ini_date + " to " + end_date
labels = [xlabel,title,ylabel,4]
max_y = np.max((np.max(x_All_PD),np.max(AllData[:,ER])))
Plots.PLOT_CALIB_DATA_SENSORS(x,AllData[:,ER],x_All_PD,file_wr,i,labels,'TRUE',max_y)

# save the model to disk
filename = dir_wr + "/" +  raptor_name + "-s" + str(i) + '-Model.sav'
pickle.dump(model, open(filename, 'wb'))

# Writing the trainMean and trainSD to a file
cap_NormStats = dir_wr + "/" +  raptor_name + ".NormStats.txt"
train_stats=np.matrix([trainMean,trainSd])
header="First row is trainMean, second row is trainSd"
np.savetxt(cap_NormStats,train_stats,fmt='%f',header=header)


# Writing the results, parameters and performance values to a file
res_list = []
res_list.append(['RAPTOR NAME: ',raptor_name,'Sensors to calibrate',sensors_cal])
res_list.append(['Size of Data','Size of Training Data', 'Size of Test Data'])
res_list.append([len(AllData),len(trainData),len(testData)])
res_list.append(['Train RMSE'])
res_list.append(["%.4f"%f for f in train_RMSE_s])
res_list.append(['Test RMSE'])
res_list.append(["%.4f"%f for f in test_RMSE_s])
res_list.append(['All Data RMSE'])
res_list.append(["%.4f"%f for f in tot_RMSE_s])
res_list.append(['Train R2'])
res_list.append(["%.4f"%f for f in train_R2_s])
res_list.append(['Test R2'])
res_list.append(["%.4f"%f for f in test_R2_s])
res_list.append(['All Data R2'])
res_list.append(["%.4f"%f for f in tot_R2_s])
res_list.append(['Training time'])
res_list.append(["%.4f"%f for f in train_time_s])
res_list.append(['Best ntrees'])
res_list.append(["%.4f"%f for f in model_ntree_s])
res_list.append(['Best max_vars'])
res_list.append(["%.4f"%f for f in model_nvar_s])
res_list.append(['Best max_depth'])
res_list.append(["%.4f"%f for f in model_depth_s])
res_list.append(['CV MSE'])
res_list.append(["%.4f"%f for f in cv_s])
res_list.append(['y train'])
res_list.append([["%.4f"%f for f in x ]for x in y_train_s])
res_list.append(['y hat train'])
res_list.append([["%.4f"%f for f in x] for x in y_hat_train_s])
res_list.append(['y test'])
res_list.append([["%.4f"%f for f in x ]for x in y_test_s])
res_list.append(['y hat test'])
res_list.append([["%.4f"%f for f in x ]for x in  y_hat_test_s])
# Resulting file : RF-QoI-metrics-(raptor_name).csv
file_w = dir_path + "/RF-QoI-metrics-" + raptor_name + ".csv"
with open(file_w,"w") as fw:
    writer=csv.writer(fw,delimiter=';',skipinitialspace=True,quotechar="'")
    for j in range(0,len(res_list)):
        writer.writerow(res_list[j])
