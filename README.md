# Sensor Calibration #

This repository contains python scripts to calibrate tropospheric ozone sensor data obtained in the H2020 Captor project. Four different models are implemented; Multiple Linear Regression (MLR), K-Nearest Neighbors (KNN),Random Forest(RF) and Support Vector Regression (SVR).  The different scripts can calibrate CAPTORS and RAPTORS node sensors (depending on the file name) using a linear or non-linear method. These files were used as the basis for the results of the article: "A comparative study of calibration methods for low-cost ozone sensors in IoT platoform" submitted to the IEEE Internet of Things journal. File Calib_CAPTOR_Classes.py is a library that contains auxiliary functions used within the calibration scripts. 

### Script execution examples ###

Depending on whether you want to perform the calibration of a MOX sensor of a Captor or an EC of a Raptor you have to use one script or another (Method_{CAPTOR/RAPTOR}_calib.py). Next, there is the command to calibrate sensor 1 and 2 of captor 17016 with the Random Forest method:

$ python RF_CAPTOR-calib.py -f CAP-17016_TodoVic.txt -n C17016 -p VIC -s 12 -t 4 -w 75

To calibrate the EC sensor of the 2018 Raptor 69 with the K-Nearest Neighbors:

$ python KNN_RAPTOR-calib.py -f R69-18.csv -n R69 -p Italy -s 2 -and 2018

For more information about the input parameters of the scripts use the following command:

$ python MLR_CAPTOR_calib.py --help


### Data ###

All the data used in the paper elaboration, as well as the long-term predictions can be found at Zenodo https://doi.org/10.5281/zenodo.3233516 . There, you can find instructions on how to get the concentrations of ozone from the different reference stations. These values must be placed after the timestamp field on each data file to be able to execute the calibration scripts.
